# <i>Expecto Patronum!</i>


## Purpose of the project

This project aims to produce an alternative Patronus test to that available on [WizardingWorld](https://www.wizardingworld.com/patronus). Why? Because the test written by J.K. Rowling is a loosely loaded die (and a d133 at that!). Chances are, the Patronus that best matches your answers had around 5% chances of being your result. Your Patronus changing over time is not due to a change of character, it's what is expected to happen when you roll a die several times in a row!

Call it rigidity, call it ASD, here I am rebuilding a Patronus test because why not! Also, it's a perfect opportunity for me to learn about Shiny, about version control, and many other stuff.

In advance, please excuse the amateur state of everything you'll find here! I am only starting my journey.


## How to use this Patronus test?

Not possible yet, still in development.


## Contributing

I am very open to suggestions and help of any kind, be it advice or participation.


## Authors and acknowledgment
The original Patronus test was developed by J.K. Rowling, and made available on Pottermore (now [WizardingWorld](https://www.wizardingworld.com/)).
This alternative Patronus test was developed by myself, based on the breakdown of every routes detailed by [/u/Alolakazam on reddit/r/harrypotter](https://www.reddit.com/r/harrypotter/comments/54hjcv/the_complete_pottermore_patronus_quiz_breakdown/?rdt=42286).
